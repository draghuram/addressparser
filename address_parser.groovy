
import java.util.regex.Matcher
import java.util.regex.Pattern

addressesData = [
    [
        address: "101/3910, KOALA BARK DR, NEW CANBERRA, WA 1234",
        postcode: 1234, state: "WA", city: "NEW CANBERRA",
        street: "KOALA BARK DR"
    ],

    [
        address: "200, Broadway Av, WEST BEACH, SA 5024",
        postcode: 5024, state: "SA", city: "WEST BEACH",
        street: "Broadway Av"
    ],

    [
        address: "SUITE 109 FLOOR 5 240, WANDALOO ESP, NEW CANBERRA, WA 1234",
        postcode: 1234, state: "WA", city: "NEW CANBERRA",
        street: "WANDALOO ESP"
    ],

    [
        address: "U 235 201-203, BROADWAY AVE, WEST BEACH, SA 5024",
        postcode: 5024, state: "SA", city: "WEST BEACH",
        street: "BROADWAY AVE"
    ]
]

class AustraliaAddressParser {
    int postcode
    String state
    String city
    String street

    def AustraliaAddressParser(String address) {
        Pattern pattern = Pattern.compile("([^,]+), ([^,]+), ([^,]+), ([^ ]+) (\\d+)")
        Matcher m = pattern.matcher(address)

        if (m.find()) {
            street = m.group(2)
            city = m.group(3)
            state = m.group(4)
            postcode = Integer.valueOf(m.group(5))
        }
    }
}

for (addressData in addressesData) {
    def parser = new AustraliaAddressParser(addressData.address)
    assert parser.postcode == addressData.postcode
    assert parser.state == addressData.state
    assert parser.city == addressData.city
    assert parser.street == addressData.street
}
